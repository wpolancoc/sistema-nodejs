var mysql = require('mysql');
var dateformat = require('dateformat')
//Productos controller
module.exports = {
  //funciones del controlador
  getProductos : function(req, res, next){
    var config = require('../database/config');
    var db = mysql.createConnection(config);
    db.connect();
    var productos = null;
    db.query('SELECT * FROM productos order by id_producto',function(err, rows, fields){
      if(err) throw err;
      productos = rows;
      db.end();
      res.render('productos/verProductos',{productos: productos});
    });
  },

  postBuscarProductos : function(req, res, next){
    var config = require('../database/config');

    var db = mysql.createConnection(config);
    db.connect();
    var nombre = req.body.nombre;
    console.log(nombre);
    var sql = 'SELECT * FROM productos WHERE nombre LIKE ?';
    var param = [nombre];
    sql = mysql.format(sql, '%'+param+'%');

    var productos = null;
    db.query(sql, function(err, rows, fields){
      if(err) throw err;

      productos = rows;
      console.log(productos);

      db.end();
      res.render('productos/verproductos',{productos: productos});
    });
  },

  getNuevoProducto : function(req, res, next){
      res.render('productos/nuevo');
  },

  postNuevoProducto : function(req, res, next){
    //    console.log(req.body);
    var fecha_actual = new Date();
    var fecha = dateformat(fecha_actual, 'yyyy-mm-dd h:MM:ss');

    var producto = {
      nombre : req.body.nombre,
      precio : req.body.precio,
      stock : req.body.stock,
      fecha_creacion: fecha
    }
    //console.log(producto);

    var config = require('.././database/config');
    var db = mysql.createConnection(config);
    db.connect();
    db.query('INSERT INTO productos SET ?', producto, function(err, rows, fields){
      if(err) throw err;

      db.end();

    });

    res.render('productos/nuevo', {info: 'Producto creado exitosamente'});
  },

  eliminarProducto : function(req, res, error){
    var id = req.body.id;
    var config = require('.././database/config');
    var db = mysql.createConnection(config);
    db.connect();

    var respuesta = {res: false};

    db.query('DELETE FROM productos WHERE id_producto=?', id, function(err,rows, fields){
      if(err)throw err;

      db.end();
      respuesta.res= true;
      res.json(respuesta);
    });

  },

  getModificarProducto : function(req, res, next){
    var id = req.params.id;
    var config = require('.././database/config');
    var db = mysql.createConnection(config);
    db.connect();

    var producto = null;

    db.query('SELECT * FROM productos WHERE id_producto = ?', id, function(err, rows, fields){
        if(err) throw err;

        producto = rows;
        db.end();
        res.render('productos/modificar', {producto: producto});
    });
  },

  postModificarProducto : function(req, res, next){

    var producto = {
      nombre : req.body.nombre,
      precio: req.body.precio,
      stock: req.body.stock,
    };

    var config = require('.././database/config');
    var db = mysql.createConnection(config);
    db.connect();

    db.query('UPDATE productos SET ? WHERE ?', [producto, {id_producto: req.body.id_producto}], function(err, rows, fields){
      if(err) throw err;

      db.end();

    });

    res.redirect('/productos');
  }

}
