-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 18-05-2018 a las 20:11:39
-- Versión del servidor: 5.7.21
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `precio` float NOT NULL,
  `stock` int(11) NOT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_producto`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre`, `precio`, `stock`, `fecha_creacion`) VALUES
(1, 'IPHONE 8', 520.32, 25, '2018-02-10 10:55:20'),
(2, 'APPLE WATCH 3', 415.2, 150, '2018-02-10 10:59:20'),
(9, 'IPAD AIR ', 580.65, 65, '2018-05-13 01:18:26'),
(14, 'Reloj Casio', 58.65, 3, '2018-05-13 06:45:13'),
(13, 'GOOGLE GLASS', 1100, 2, '2018-05-13 05:38:16'),
(12, 'AIR PODS', 125.95, 40, '2018-05-13 02:47:50'),
(15, 'Mouse óptico', 15.2, 50, '2018-05-13 07:59:13'),
(16, 'Wireless Keyboard', 15.2, 25, '2018-05-13 08:29:26'),
(17, 'Joystick ', 29.2, 20, '2018-05-13 08:29:50'),
(18, 'Gamer PC', 680.24, 13, '2018-05-13 08:30:07'),
(19, 'Laptop Adapter 16W', 21.05, 20, '2018-05-13 08:30:42'),
(20, 'hola', 55, 66, '2018-05-14 04:58:46');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
